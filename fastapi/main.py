import os
from dotenv import load_dotenv
import uvicorn
from fastapi import FastAPI, Depends, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import jwt
from pymongo import MongoClient
from langchain_community.vectorstores import FAISS
from langchain_community.embeddings import HuggingFaceBgeEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.docstore.document import Document
from fastapi.security import OAuth2PasswordBearer
from bson import ObjectId
from sentence_transformers import SentenceTransformer
from sklearn.metrics.pairwise import cosine_similarity

client = MongoClient("mongodb://localhost:27017/")
try:
    server_info = client.server_info()
except Exception as e:

db = client["notesarray"]
users_collection = db["users"]

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

class SearchRequest(BaseModel):
    query: str

def verify_token(token: str = Depends(oauth2_scheme)):
    load_dotenv()
    jwt_secret_key = os.getenv("JWT_SECRET_KEY")
    if jwt_secret_key is None:
        raise HTTPException(status_code=500, detail="JWT secret key not found in environment variables")
    
    try:
        payload = jwt.decode(token, jwt_secret_key, algorithms=["HS256"])
        user_id = payload.get("id")
        print(user_id)
        if user_id is None:
            raise HTTPException(status_code=401, detail="Invalid token")
    except jwt.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Token expired")
    except jwt.InvalidTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")
    
    return user_id

def get_user_notes(user_id):
    try:
        oid = ObjectId(user_id)
    except Exception as e:
        raise HTTPException(status_code=400, detail="Invalid user ID format")

    user = users_collection.find_one({"_id": oid})
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")
    
    notes = user.get("notes", [])
    return notes

def search_notes(query, notes):
    model = SentenceTransformer('distiluse-base-multilingual-cased-v1')
    note_embeddings = model.encode([f"{note.get('title', '')} {note.get('body', '')}" for note in notes])
    query_embedding = model.encode([query])[0]
    similarities = cosine_similarity([query_embedding], note_embeddings)[0]
    top_indices = similarities.argsort()[-len(notes):][::-1]
    relevant_notes = [notes[i] for i in top_indices[:5]]
    for note in relevant_notes:
        if '_id' in note:
            note['_id'] = str(note['_id'])

    return relevant_notes

@app.post("/search")
def search(request: SearchRequest, user_id=Depends(verify_token)):
    notes = get_user_notes(user_id)
    relevant_notes = search_notes(request.query, notes)
    return {"relevant_notes": relevant_notes}

if __name__ == "__main__":
    uvicorn.run("main:app", host="172.17.0.1", port=5000, reload=True)
