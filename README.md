# SmartNoteAI

SmartNoteAI — это веб-приложение, созданное на Vue.js, которое позволяет пользователям сохранять заметки и выполнять релевантный поиск с использованием моделей обработки естественного языка (LLM) в базе данных. Бэкенд приложения построен на FastAPI и MongoDB, что обеспечивает надежную и эффективную среду для управления пользовательскими заметками и функциями поиска. Обзор на YouTube [https://youtu.be/7fAWabHOnlA](https://youtu.be/7fAWabHOnlA)

## Особенности

- **Аутентификация пользователей**: Безопасная аутентификация с использованием JWT.
- **Создание и сохранение заметок**: Пользователи могут создавать и сохранять свои заметки.
- **Релевантный поиск**: Выполнение релевантного поиска по заметкам пользователей с использованием моделей обработки естественного языка.
- **Адаптивный дизайн**: Мобильная дружественность интерфейса.

## Технологии

- **Фронтенд**: Vue.js
- **Бэкенд**: FastAPI
- **База данных**: MongoDB
- **Другие библиотеки**:
  - `dotenv` для работы с переменными окружения
  - `fastapi.middleware.cors` для поддержки CORS
  - `pydantic` для валидации данных
  - `jwt` для работы с JSON Web Tokens
  - `pymongo` для работы с MongoDB
  - `sentence_transformers` и `sklearn` для обработки и поиска заметок
