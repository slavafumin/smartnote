const ApiError = require('../exceptions/api-error');
const tokenService = require('../service/token-service');
const User = require('../models/user-model'); // Импорт модели User

module.exports = async function (req, res, next) {
  try {
    const authorizationHeader = req.headers.authorization;
    if (!authorizationHeader) {
      return next(ApiError.UnauthorizedError());
    }

    const accessToken = authorizationHeader.split(' ')[1];
    if (!accessToken) {
      return next(ApiError.UnauthorizedError());
    }

    const userData = tokenService.validateAccessToken(accessToken);
    if (!userData) {
      return next(ApiError.UnauthorizedError());
    }

    // Получаем пользователя из базы данных по ID из userData
    const user = await User.findById(userData.id);
    if (!user) {
      return next(ApiError.UnauthorizedError());
    }
    console.log(user.isPaid)
    // Проверяем, оплатил ли пользователь
    if (!user.isPaid) {
      return next(ApiError.ForbiddenError('Доступ запрещен. Требуется оплата.'));
    }

    req.user = user; // Сохраняем весь объект пользователя, если это необходимо, или только userData
    next();
  } catch (e) {
    return next(ApiError.UnauthorizedError());
  }
}
