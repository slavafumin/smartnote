const { Schema, model } = require('mongoose');

const NoteSchema = new Schema({
  title: { type: String },
  body: { type: String, required: true },
});

const UserSchema = new Schema({
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  isActivated: { type: Boolean, default: false },
  activationLink: { type: String, required: true },
  isPaid: { type: Boolean, default: false },
  notes: [NoteSchema] 
});

module.exports = model('User', UserSchema);
