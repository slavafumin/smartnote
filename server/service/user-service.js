const UserModel = require('../models/user-model')
const TokenModel = require('../models/token-model');
const bcrypt = require('bcrypt')
const uuid = require('uuid')
const mailService = require('./mail-service')
const tokenService = require('./token-service')
const UserDto = require('../dtos/user-dto');
const ApiError = require('../exceptions/api-error');
const fs = require('fs');

class UserService {
  async registration(email, password) {
    const candidate = await UserModel.findOne({email})
    if (candidate) {
      throw ApiError.BadRequest(`Пользователь с email ${email} уже существует`)
    }
    const hashPassword = await bcrypt.hash(password, 3)
    const activationLink = uuid.v4()

    const user = await UserModel.create({email, password: hashPassword, activationLink})
    await mailService.sendActivationMail(email, `${process.env.API_URL}/api/activate/${activationLink}`)
    
    const userDto = new UserDto(user)
    const tokens = tokenService.generateTokens({...userDto})
    await tokenService.saveToken(userDto.id, tokens.refreshToken)

    return {...tokens, user: userDto}
  }

  async resetPassword(email) {
    const user = await UserModel.findOne({email})
    if (!user) {
        throw ApiError.NotFound('Пользователь с таким email не найден')
    }
     // Находим токен для сброса пароля по _id пользователя
      // const userIdString = user._id.toString(); 
    //   const token = await TokenModel.findOne({ user: user._id });
    //  if (!token) {
    //      throw ApiError.BadRequest('Токен для сброса пароля не найден');
    //  }

     // Используем refreshToken для генерации ссылки для сброса пароля
     const refreshToken = user.password;
    await mailService.sendResetPasswordMail(email, refreshToken)
    return { success: true, message: 'Письмо для изменения пароля отправлено' };
  } catch (error) {
    return { success: false, message: error.message };
  }

  async newPassword(resetToken, newPassword) {
    console.log(resetToken, newPassword)
    const user = await UserModel.findOne({password: resetToken})
    console.log(user)
    if (!user) {
        throw ApiError.BadRequest('Неверный токен сброса пароля')
    }
    // Генерируем новый хэш пароля
    const hashPassword = await bcrypt.hash(newPassword, 3)
    // Обновляем пароль пользователя в базе данных
    // const userIdString = userToken.user.toString();
    // console.log(userIdString)
    await UserModel.updateOne({_id: user._id}, {password: hashPassword})

    return { success: true, message: 'Пароль успешно именен' };
  } catch (error) {
    return { success: false, message: error.message };
  }


  async activate(activationLink) {
    const user = await UserModel.findOne({activationLink})
    if (!user) {
      throw ApiError.BadRequest('Некорректная ссылка активации')
    }
    user.isActivated = true
    await user.save()
  }

  async login(email, password) {
    const user = await UserModel.findOne({email})
    if (!user) {
      throw ApiError.BadRequest('Пользователь с таким email не найден')
    }
    const isPassEquals = await bcrypt.compare(password, user.password)
    if (!isPassEquals) {
      throw ApiError.BadRequest('Неверный пароль')
    }
    const userDto = new UserDto(user)
    const tokens = tokenService.generateTokens({...userDto})

    await tokenService.saveToken(userDto.id, tokens.refreshToken)

    return {...tokens, user: userDto}
  }

  async logout(refreshToken) {
    const token = await tokenService.removeToken(refreshToken)
    return token
  }

  async refresh(refreshToken) {
    if (!refreshToken) {
      throw ApiError.UnauthorizedError()
    }
    const userData = tokenService.validateRefreshToken(refreshToken)
    const tokenFromDb = await tokenService.findToken(refreshToken)
    if (!userData || !tokenFromDb) {
      throw ApiError.UnauthorizedError()
    }
    const user = await UserModel.findById(userData.id)
    const userDto = new UserDto(user)
    const tokens = tokenService.generateTokens({...userDto})

    await tokenService.saveToken(userDto.id, tokens.refreshToken)

    return {...tokens, user: userDto}
  }

  async getAllUsers(userId) {
    const users = await UserModel.findById(userId)
    return users
  }

  async saveNote(userId, noteData) {
    try {
      // Находим пользователя по userId
      const user = await UserModel.findById(userId);
  
      if (!user) {
        // Обработка случая, если пользователь не найден
        throw new Error('Пользователь не найден');
      }
  
      // Создаем новую заметку на основе данных
      const newNote = {
        title: noteData.title,
        body: noteData.body
      };
  
      // Добавляем новую заметку в массив заметок пользователя
      user.notes.push(newNote);
  
      // Сохраняем обновленного пользователя в базе данных
      await user.save();
  
      // Получаем добавленную заметку с _id
      const savedNote = user.notes[user.notes.length - 1];

      // Возвращаем результат, включая полный объект заметки с _id
      return { success: true, message: 'Заметка успешно сохранена', note: savedNote };
    } catch (error) {
      // Обработка ошибок при сохранении заметки
      return { success: false, error: error.message };
    }
}

  async removeNote(userId, noteId) {
    try {
      const user = await UserModel.findById(userId);
  
      if (!user) {
        throw new Error('Пользователь не найден');
      }
  
      user.notes = user.notes.filter(note => note._id.toString() !== noteId);
      await user.save();
  
      return { success: true, message: 'Заметка успешно удалена' };
    } catch (error) {
      return { success: false, error: error.message };
    }
  }

  async getNote(userId, noteId) {
    try {
      const user = await UserModel.findById(userId);
  
      if (!user) {
        throw new Error('Пользователь не найден');
      }
  
      const note = user.notes.find(note => note._id.toString() === noteId);

      if (!note) {
          throw new Error('Заметка не найдена');
      }

      return { success: true, message: 'Заметка успешно удалена', note: note };
    } catch (error) {
      return { success: false, error: error.message };
    }
  }
  
  async updateNote(userId, noteId, newBodyNote) {
    try {
      // Находим пользователя по его идентификатору
      const user = await UserModel.findById(userId);
  
      if (!user) {
        throw new Error('Пользователь не найден');
      }
  
      // Находим заметку пользователя по ее _id
      const note = user.notes.find(note => note._id.toString() === noteId);
  
      if (!note) {
        throw new Error('Заметка не найдена');
      }
  
      // Вносим изменения в поле body заметки
      note.body = newBodyNote;
  
      // Сохраняем обновленного пользователя в базу данных
      const updatedUser = await user.save();
  
      // Находим обновленную заметку в массиве notes обновленного пользователя
      const updatedNote = updatedUser.notes.find(note => note._id.toString() === noteId);
  
      return { success: true, message: 'Заметка успешно обновлена', note: updatedNote };
    } catch (error) {
      return { success: false, message: error.message };
    }
  }

  async uploadZip(userId, zipEntries) {
    try {
        // Находим пользователя по его идентификатору
        const user = await UserModel.findById(userId);

        if (!user) {
            throw new Error('Пользователь не найден');
        }

        // Создаем массив для хранения заметок
        const notes = [];

        // Обрабатываем каждый элемент в архиве
        for (const zipEntry of zipEntries) {
            // Если файл - Markdown
            if (zipEntry.entryName.endsWith('.md')) {
                const title = decodeURIComponent(zipEntry.entryName.replace('.md', '')); // Имя файла без расширения
                
                // Проверяем, есть ли уже заметка с таким заголовком
                const existingNote = user.notes.find(note => note.title === title);
                
                // Если нет заметки с таким заголовком, добавляем заметку в массив
                if (!existingNote) {
                    const body = zipEntry.getData().toString('utf8'); // Содержимое файла
                    notes.push({ title, body });
                }
            }
        }

        // Если есть новые заметки, обновляем заметки пользователя в базе данных
        if (notes.length > 0) {
            await UserModel.findByIdAndUpdate(userId, { $push: { notes: { $each: notes } } });
        }

        return { success: true, message: 'Заметки MD успешно загружены' };
    } catch (error) {
        console.error('Error uploading ZIP file:', error);
        return { success: false, error: error.message };
    }
}

  async downloadNotes(userId) {
    try {
        const user = await UserModel.findById(userId);

        if (!user) {
            throw new Error('Пользователь не найден');
        }

        const notes = user.notes.map(note => ({
            title: note.title,
            body: note.body
        }));

        return { success: true, message: 'Заметки успешно загружены', notes: notes };
    } catch (error) {
        return { success: false, error: error.message };
    }
}

  
}

module.exports = new UserService()