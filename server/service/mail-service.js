const nodemailer = require('nodemailer')

class MailService {
  constructor() {
    this.transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: true,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD
      }
    })
  }
  
  async sendActivationMail(to, link) {
    await this.transporter.sendMail({
      from: process.env.SMTP_USER,
      to,
      subject: 'Активация аккаунта на ' + process.env.API_URL,
      text: '',
      html:
        `
        <div>
          <h1>Для Активация перейдите по ссылке</h1>
          <a href="${link}">${link}</a>
        </div>
        `
      })
    }


// Функция для отправки письма для сброса пароля
  async sendResetPasswordMail(email, refreshToken) {
    await this.transporter.sendMail({
        from: process.env.SMTP_USER,
        to: email,
        subject: 'Сброс пароля' + process.env.API_URL,
        text: `Для сброса пароля перейдите по ссылке: ${process.env.API_URL}/new-password?token=${refreshToken}`
    })
        
  }
}

module.exports = new MailService()