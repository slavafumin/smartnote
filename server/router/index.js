const Router = require('express').Router
const userController = require('../controllers/user-controller')
const router = new Router()
const {body} = require('express-validator');
const authMiddleware = require('../middlewares/auth-middleware');
const multer = require('multer');

const upload = multer({ 
  dest: 'uploads/',
  limits: { fileSize: 5 * 1024 * 1024 } // Ограничение до 5 мегабайт
 });

router.post('/registration', 
  body('email').isEmail(),
  body('password').isLength({min: 6, max: 32}),
  userController.registration
)
router.post('/new-password/', 
  body('token'),
  body('newPassword').isLength({min: 6, max: 32}),
  userController.newPassword
)
router.post('/login', userController.login)
router.post('/logout', userController.logout)
router.post('/saveNote', authMiddleware, userController.saveNote)
router.post('/updateNote/:noteId', authMiddleware, userController.updateNote)
router.post('/reset-password/', body('email').isEmail(), userController.resetPassword)
router.post('/upload/', authMiddleware, upload.single('file'), userController.uploadZip)
router.get('/activate/:link', userController.activate)
router.get('/refresh', userController.refresh)
router.get('/users/', authMiddleware, userController.getUsers)
router.get('/note/:noteId', authMiddleware, userController.getNote)
router.get('/downloadNotes/', authMiddleware, userController.downloadNotes)
router.delete('/remove-note/:noteId', authMiddleware, userController.removeNote);


module.exports = router

