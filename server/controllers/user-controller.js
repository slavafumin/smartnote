const { validationResult } = require('express-validator');
const userService = require('../service/user-service');
const ApiError = require('../exceptions/api-error');
const jwt = require('jsonwebtoken')
const AdmZip = require('adm-zip');
const fs = require('fs');

class UserController {
  async registration(req, res, next) {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return next(ApiError.BadRequest('Ошибка при валидации', errors.array()))
      }
      const {email, password} = req.body
      const userData = await userService.registration(email, password)
      res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})
      return res.json(userData)
    } catch (e) {
      next(e)
    }
  }

  async resetPassword(req, res, next) {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return next(ApiError.BadRequest('Ошибка при валидации', errors.array()))
      }
      const {email} = req.body
      const { success, message } = await userService.resetPassword(email)
      return res.status(200).json({success, message})
    } catch(e) {
      next(e)
    }
  }

  async newPassword(req, res, next) {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return next(ApiError.BadRequest('Ошибка при валидации', errors.array()))
      }
      const { token, newPassword } = req.body
      const { success, message } = await userService.newPassword(token, newPassword)
      return res.status(200).json({success, message})
    } catch (e) {
      next(e)
    }
  }

  async login(req, res, next) {
    try {
      const {email, password} = req.body
      const userData = await userService.login(email, password)
      res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})
      return res.json(userData)
    } catch (e) {
      next(e)
    }
  }
  async logout(req, res, next) {
    try {
      const {refreshToken} = req.cookies
      const token = await userService.logout(refreshToken)
      res.clearCookie('refreshToken')
      return res.json(token)
    } catch (e) {
      next(e)
    }
  }
  async activate(req, res, next) {
    try {
      const activationLink = req.params.link
      await userService.activate(activationLink)
      return res.redirect(process.env.CLIENT_URL)
    } catch (e) {
      next(e)
    }
  }

  async refresh(req, res, next) {
    try {
      const {refreshToken} = req.cookies
      const userData = await userService.refresh(email, password)
      res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})
      return res.json(userData)
  } catch (e) {
    next(e)
  }
}

  async getUsers(req, res, next) {
    try {
      const token = req.headers.authorization.split(' ')[1]; // Получаем токен из заголовка Authorization
      const decodedToken = jwt.decode(token); 
      const userId = decodedToken.id;
      const users = await userService.getAllUsers(userId)
      const notes = users.notes
      return res.json(notes)
  } catch (e) {
    next(e)
  }
  }

  async saveNote(req, res, next) {
    try {
      const token = req.headers.authorization.split(' ')[1]; // Получаем токен из заголовка Authorization
      const decodedToken = jwt.decode(token);
      const userId = decodedToken.id;
      const { success, message, note } = await userService.saveNote(userId, req.body); // Получаем полный объект заметки
  
      if (!success) {
        return res.status(400).json({ message }); // Возвращаем сообщение об ошибке, если сохранение не удалось
      }
  
      res.status(200).json({ message, note }); // Возвращаем успешное сообщение и полный объект заметки
    } catch (e) {
      next(e);
    }
  }
  
  async removeNote(req, res, next) {
    try {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const userId = decodedToken.id;
      const noteId = req.params.noteId;
  
      const { success, message } = await userService.removeNote(userId, noteId);
  
      if (!success) {
        return res.status(400).json({ message });
      }
  
      res.status(200).json({ message });
    } catch (e) {
      next(e);
    }
  }

  async getNote(req, res, next) {
    try {
      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.decode(token);
      const userId = decodedToken.id;
      const noteId = req.params.noteId;
  
      const { success, message, note } = await userService.getNote(userId, noteId);
  
      if (!success) {
        return res.status(400).json({ message });
      }
      res.status(200).json({ message, note });
    } catch (e) {
      next(e);
    }
  };

  async updateNote(req, res) {
    try {
      const token = req.headers.authorization.split(' ')[1]; // Получаем токен из заголовка Authorization
      const decodedToken = jwt.decode(token);
      const userId = decodedToken.id;
      const noteId = req.params.noteId
      const newBodyNote = req.body.body
      const { success, message, note } = await userService.updateNote(userId, noteId, newBodyNote); // Получаем полный объект заметки
  
      if (!success) {
        return res.status(400).json({ message }); // Возвращаем сообщение об ошибке, если сохранение не удалось
      }
  
      res.status(200).json({ message, note }); // Возвращаем успешное сообщение и полный объект заметки
    } catch (error) {
      return { success: false, message: 'Произошла ошибка при сохранении заметки' };
    }
  }

  async uploadZip(req, res) {
    try {
      const token = req.headers.authorization.split(' ')[1]; // Получаем токен из заголовка Authorization
      const decodedToken = jwt.decode(token);
      const userId = decodedToken.id;
      const zip = new AdmZip(req.file.path);
      const zipEntries = zip.getEntries();
      const { success, message, note } = await userService.uploadZip(userId, zipEntries); // Получаем полный объект заметки
      fs.unlink(req.file.path, (err) => {
        if (err) {
            console.error('Error deleting uploaded file:', err);
        } else {
            console.log('Uploaded file deleted successfully');
        }
    });

      if (!success) {
        return res.status(400).json({ message }); // Возвращаем сообщение об ошибке, если сохранение не удалось
      }
  
      res.status(200).json({ success: true, message }); // Возвращаем успешное сообщение и полный объект заметки
    } catch (error) {
      return { success: false, message: 'Произошла ошибка при сохранении заметки' };
    }
  }
  

  async downloadNotes(req, res) {
    try {
      const token = req.headers.authorization.split(' ')[1]; // Получаем токен из заголовка Authorization
      const decodedToken = jwt.decode(token);
      const userId = decodedToken.id;
      const { success, message, notes } = await userService.downloadNotes(userId); // Получаем заметки пользователя
  
      if (!success) {
        return res.status(400).json({ message }); // Возвращаем сообщение об ошибке, если загрузка заметок не удалась
      }
  
      res.setHeader('Content-Type', 'application/octet-stream'); // Устанавливаем правильный Content-Type
      res.status(200).json({ notes }); // Возвращаем успешное сообщение и заметки пользователя
    } catch (error) {
      console.error('Произошла ошибка при загрузке заметок:', error);
      res.status(500).json({ success: false, message: 'Произошла ошибка при загрузке заметок' });
    }
  }
}  

module.exports = new UserController()