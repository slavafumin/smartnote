my-project/
|-- backend/             # Серверная часть (Express.js)
|   |-- controllers/     # Контроллеры для обработки запросов
|   |-- models/          # Модели данных
|   |-- routes/          # Маршруты (API endpoints)
|   |-- app.js           # Основной файл сервера Express
|   |-- package.json     # Файл зависимостей для Express.js
|
|-- frontend/            # Клиентская часть (Vue.js)
|   |-- public/         # Статические ресурсы
|   |-- src/            # Исходный код Vue.js приложения
|   |-- ...
|   |-- package.json    # Файл зависимостей для Vue.js
|
|-- package.json         # Главный файл зависимостей для всего проекта
