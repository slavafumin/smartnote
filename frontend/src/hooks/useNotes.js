import { ref, onMounted } from 'vue';
import axios from "axios";

export function useNotes() {
    const notes = ref([]);
    const isNotesLoading = ref(true);

    const fetching = async () => {
        try {
            const token = localStorage.getItem('token'); 
            const response = await axios.get('/api/users', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            notes.value = response.data;
        } catch (error) {
            console.error('Ошибка при получении данных:', error);
            // Обработка ошибок, например, показ сообщения пользователю
        } finally {
            isNotesLoading.value = false;
        }
    };

    onMounted(fetching);

    return {
        notes,
        isNotesLoading
    };
}
